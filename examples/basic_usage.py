from wulffpack import (SingleCrystal,
                       Decahedron,
                       Icosahedron)
from ase.build import bulk
from ase.io import write

# Show a regular Wulff construction, cubic crystal
surface_energies = {(1, 0, 0): 1.1,
                    (1, 1, 1): 1.,
                    (1, 1, 0): 1.15,
                    (2, 1, 1): 1.14}
particle = SingleCrystal(surface_energies)
particle.view()
write('single_crystal.xyz', particle.atoms)

# Wulff construction for hcp crystal
prim = bulk('Co', crystalstructure='hcp')
surface_energies = {(1, 0, -1, 0): 1.1,
                    (0, 0, 0, 1): 1.0,
                    (1, 1, -2, 0): 1.0}
particle = SingleCrystal(surface_energies,
                         primitive_structure=prim,
                         natoms=5000)
particle.view()
write('hcp_single_crystal.xyz', particle.atoms)

# Change number of atoms
particle.natoms = 10000
write('hcp_single_crystal_larger.xyz', particle.atoms)

# Wulff construction for decahedron
surface_energies = {(1, 0, 0): 1.1,
                    (1, 1, 1): 1.,
                    (1, 1, 0): 1.15}
prim = bulk('Pd', a=3.9)
particle = Decahedron(surface_energies,
                      twin_energy=0.04,
                      primitive_structure=prim)
particle.view()
write('decahedron.xyz', particle.atoms)

# Wulff construction for icosahedron
particle = Icosahedron(surface_energies,
                       twin_energy=0.04,
                       primitive_structure=prim)
particle.view()
write('icosahedron.xyz', particle.atoms)
