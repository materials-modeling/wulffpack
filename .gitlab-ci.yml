image: python:3.9

include:
  - template: Code-Quality.gitlab-ci.yml

variables:
  INSTDIR: "local_installation"

before_script:
  - export PYTHONPATH=$PWD/$INSTDIR:${PYTHONPATH}


#------------------- build stage -------------------

build:
  stage: build
  script:
    - pip3 install --target=$INSTDIR .
    - echo "import site; site.addsitedir('${INSTDIR}')" > $INSTDIR/sitecustomize.py
  artifacts:
    expire_in: 2 days
    paths:
      - local_installation/
  tags:
    - linux


#------------------- test stage -------------------

basic_tests:
  stage: test
  script:
    - python -m pip install coverage pytest xdoctest
    - xdoctest wulffpack
    - coverage run -m pytest --verbose --junitxml=report.xml tests/
    - coverage report -m
    - coverage html
  needs:
    - build
  tags:
    - linux
  coverage: '/TOTAL.+ ([0-9]{1,3}%)/'
  artifacts:
    expire_in: 2 days
    paths:
      - htmlcov/
    reports:
      junit: report.xml
  
style_check:
  stage: test
  tags:
    - linux
  script:
    - pip install flake8
    - flake8 wulffpack/ tests/ doc/

code_quality:
  artifacts:
    paths: [gl-code-quality-report.json]

test_documentation:
  stage: test
  tags:
    - linux
  needs:
    - build
  except:
    - master
  artifacts:
    expire_in: 1 days
    paths:
      - public
  script:
    - INSTDIR=$PWD/opt/lib/python
    - export PYTHONPATH=$INSTDIR:$PYTHONPATH
    - python3 -m pip install cloud_sptheme
    - python3 -m pip install sphinx_autodoc_typehints
    - python3 -m pip install sphinx-sitemap
    - python3 -m pip install sphinx_rtd_theme
    - sphinx-build -W doc/ public/
    - ls -l public/

#------------------- pages stage -------------------

pages:
  stage: deploy
  dependencies:
    - build
    - basic_tests
  script:
    # prepare homepage
    - mkdir -p public/dev
    # code coverage report
    - cp -dr htmlcov/ public/coverage/
    # build user guide
    - INSTDIR=$PWD/opt/lib/python
    - export PYTHONPATH=$INSTDIR:$PYTHONPATH
    - python3 -m pip install cloud_sptheme
    - python3 -m pip install sphinx_autodoc_typehints
    - python3 -m pip install sphinx-sitemap
    - python3 -m pip install sphinx_rtd_theme

    # STABLE VERSION
    - tag=$(git tag | tail -1)
    - echo "tag= $tag"
    - git checkout $tag
    - sphinx-build doc/ public/

    # make tests and examples downloadable
    - apt-get update
    - apt-get install -y zip unzip
    - find examples/ -print | zip public/examples.zip -@
    - find tests/ -print | zip public/tests.zip -@

    # DEVELOPMENT VERSION
    - git checkout master
    - tag=$(git describe | tail -1)
    - echo "tag= $tag"
    # build user guide
    - sed -i "s/version = ''/version = '$tag'/" doc/conf.py
    - grep version doc/conf.py
    - sphinx-build doc/ public/dev/

    # make tests and examples downloadable
    - find examples/ -print | zip public/dev/examples.zip -@
    - find tests/ -print | zip public/dev/tests.zip -@

    # clean up
    - ls -l public/
    - chmod go-rwX -R public/
  artifacts:
    paths:
      - public
  only:
    - master
    - tags

pypi:
  stage: deploy
  tags:
    - linux
  only:
    - tags
  when: manual
  environment:
      name: pypi-upload
  script:
    # install twine
    - python3 -m pip install twine
    # check out the latest tag (redundant if job is limited to tags; still a sensible precaution)
    - tag=$(git tag | tail -1)
    - echo "tag= $tag"
    - git checkout $tag
    # create source distribution and push to PyPI
    - python3 setup.py sdist
    - python3 setup.py bdist_wheel --universal
    - ls -l dist/
    - python3 -m twine upload dist/*
