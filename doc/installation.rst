.. _installation:
.. index:: Installation

Installation
************


Installing via `pip`
====================

In the most simple case, :program:`WulffPack` can be simply installed
via `pip`::

    pip install wulffpack

It is always a good idea to test that your installation works as advertised.
To this end, you should run the test suite, which can be accomplished
as follows::

    curl -O https://wulffpack.materialsmodeling.org/tests.zip
    unzip tests.zip
    python3 tests/main.py

Cloning the repository
======================

If you want to get the absolutely latest version you can clone the
repo::

    git clone git@gitlab.com:materials-modeling/wulffpack.git

and then install :program:`WulffPack` via::

    cd wulffpack
    python3 setup.py install --user

in the root directory. This will set up :program:`WulffPack` as a Python module
for the current user. To ensure that the installation has succeeded it is
recommended to run the tests::

    python3 tests/main.py


Requirements
============

:program:`WulffPack` requires Python3 and depends on the following libraries

* `ASE <https://wiki.fysik.dtu.dk/ase>`_ (structure handling)
* `Spglib <https://spglib.readthedocs.io/>`_ (crystal symmetries)
* `NumPy <http://www.numpy.org/>`_ (numerical linear algebra)
* `SciPy <http://www.scipy.org/>`_ (convex hull construction)
* `Matplotlib <https://matplotlib.org//>`_ (plotting)
