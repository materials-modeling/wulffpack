Decahedral particle
===================

.. autoclass:: wulffpack.Decahedron
    :members:
    :undoc-members:
    :inherited-members:
