Core components
===============

.. module:: wulffpack.core

Base particle
-------------

.. autoclass:: BaseParticle
    :members:
    :undoc-members:
    :inherited-members:

Form
----

.. autoclass:: Form
    :members:
    :undoc-members:
    :inherited-members:

Facet
-----

.. autoclass:: Facet
    :members:
    :undoc-members:
    :inherited-members:
