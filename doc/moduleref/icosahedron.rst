Icosahedral particle
====================

.. autoclass:: wulffpack.Icosahedron
    :members:
    :inherited-members:
